//
//  GroupUtilities.h
//  limit-access-to-guids
//
//  Created by Jim Zajkowski on 10/29/13.
//  Copyright (c) 2013 University of Michigan. All rights reserved.
//

#ifndef limit_access_to_guids_GIDUtilities_h
#define limit_access_to_guids_GIDUtilities_h

#import <Foundation/Foundation.h>
#import <OpenDirectory/OpenDirectory.h>

NSNumber *NextAvailableGroup(ODNode *node);
ODRecord *FindGroupByName(ODNode *node, NSString *groupName);
ODRecord *FindOrCreateGroupByName(ODNode *node, NSString *groupName);

BOOL SetNestedGroupMembershipOfGroup(ODRecord *groupRecord, NSArray *arrayOfRecords);


#endif
