//
//  nsprint.h
//  limit-access-to-guids
//
//  Created by Jim Zajkowski on 10/29/13.
//  Copyright (c) 2013 University of Michigan. All rights reserved.
//

#ifndef limit_access_to_guids_nsprint_h
#define limit_access_to_guids_nsprint_h

void NSPrint(NSString *format, ...);

#endif
