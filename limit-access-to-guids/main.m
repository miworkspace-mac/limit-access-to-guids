//
//  main.m
//  limit-access-to-guids
//
//  Created by Jim Zajkowski on 10/29/13.
//  Copyright (c) 2013 University of Michigan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSPrint.h"
#import "GroupUtilities.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool {
        BOOL success;

        // Handle insufficient arguments
        if (argc < 2)
        {
            NSPrint(@"%s: need to specify GUIDs to limit, or 'unlimited' to remove loginwindow ACL.", argv[0]);
            exit(-1);
        }

        // Convert arguments to objective C
        NSMutableArray *arguments = [NSMutableArray array];
        for (int i = 1; i < argc; i++)
        {
            [arguments addObject:[[NSString stringWithUTF8String:argv[i]] uppercaseString]];
        }

        // Find the local directory service root node
        ODSession *session = [ODSession defaultSession];
        ODNode *node = [ODNode nodeWithSession:session type:kODNodeTypeLocalNodes error:nil];

        // Find basic groups
        ODRecord *localaccounts_group = FindGroupByName(node, @"localaccounts");

        // Remove loginwindow restriction
        if (argc == 2 && [arguments[0] isEqualToString:@"UNLIMITED"])
        {
            ODRecord *netaccounts_group = FindGroupByName(node, @"netaccounts");

            // Set the com.apple.access_loginwindow group to reference "localaccounts" and "netaccounts", no others.
            ODRecord *access_loginwindow_group = FindOrCreateGroupByName(node, @"com.apple.access_loginwindow");
            success = SetNestedGroupMembershipOfGroup(access_loginwindow_group, @[ localaccounts_group, netaccounts_group ]);

            return (success ? 0 : -1);
        }

        // No, arguments is a list of GUIDs to give rights to
        else
        {
            NSMutableArray *groupsToGiveAccess = [NSMutableArray arrayWithArray:arguments];
            NSMutableArray *groupsWithLocalUsers = [NSMutableArray arrayWithArray:arguments];
            [groupsWithLocalUsers addObject:localaccounts_group];

            ODRecord *access_ssh_group = FindOrCreateGroupByName(node, @"com.apple.access_ssh");
            ODRecord *access_screensharing_group = FindOrCreateGroupByName(node, @"com.apple.access_screensharing");
            ODRecord *access_loginwindow_group = FindOrCreateGroupByName(node, @"com.apple.access_loginwindow");
            ODRecord *loginwindow_netaccounts_group = FindOrCreateGroupByName(node, @"com.apple.loginwindow.netaccounts");

            // access_ssh, includes localaccounts
            success = SetNestedGroupMembershipOfGroup(access_ssh_group, groupsWithLocalUsers);
            if (!success) {
                NSPrint(@"Failed to set com.apple.access_ssh");
                return -1;
            }

            // access_screensharing, includes localaccounts
            success = SetNestedGroupMembershipOfGroup(access_screensharing_group, groupsWithLocalUsers);
            if (!success) {
                NSPrint(@"Failed to set com.apple.access_screensharing");
                return -1;
            }

            // loginwindow.netaccounts, the groups to permit to the login window
            success = SetNestedGroupMembershipOfGroup(loginwindow_netaccounts_group, groupsToGiveAccess);
            if (!success) {
                NSPrint(@"Failed to set com.apple.loginwindow.netaccounts");
                return -1;
            }

            // access_loginwindow, which references localaccounts AND loginwindow.netaccounts
            // the GUI sets it up this way with the extra indirection
            success = SetNestedGroupMembershipOfGroup(access_loginwindow_group, @[ localaccounts_group, loginwindow_netaccounts_group ]);
            if (!success) {
                NSPrint(@"Failed to set com.apple.access_loginwindow");
                return -1;
            }

            return 0;

        }

    } // @autorelease
    return 0;
}

