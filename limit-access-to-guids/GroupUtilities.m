//
//  GroupUtilities.m
//  limit-access-to-guids
//
//  Created by Jim Zajkowski on 10/29/13.
//  Copyright (c) 2013 University of Michigan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GroupUtilities.h"

// Find the next available GID that is above MIN_GID
#define MIN_GID 450

// Private prototypes
BOOL SetNestedGroupMembershipOfGroupByGUIDs(ODRecord *groupRecord, NSArray *arrayOfGUIDs);

NSNumber *NextAvailableGroup(ODNode *node)
{
    NSError *error;

    ODQuery *query = [ODQuery queryWithNode:node
                             forRecordTypes:kODRecordTypeGroups
                                  attribute:kODAttributeTypePrimaryGroupID
                                  matchType:kODMatchAny
                                queryValues:nil
                           returnAttributes:kODAttributeTypePrimaryGroupID
                             maximumResults:1000
                                      error:&error];

    NSArray *allGroups = [query resultsAllowingPartial:NO error:&error];

    // Extract GIDs into its own array
    NSMutableArray *allGIDs = [NSMutableArray array];

    for (ODRecord *group in allGroups) {
        NSString *gidString = [[group valuesForAttribute:kODAttributeTypePrimaryGroupID error:nil] objectAtIndex:0];

        if (gidString)
        {
            [allGIDs addObject:@([gidString integerValue])];
        }
    }

    // Probe through the list until we find something
    long loops = 0;

    NSUInteger probeGID = MIN_GID;
    BOOL found = NO;
    while (!found)
    {
        // Stop if we spend too long doing this
        loops++;
        if (loops >= 1000)
        {
            @throw [NSException exceptionWithName:@"Exhausted" reason:@"Could not find a suitable GID" userInfo:nil];
        }

        // See if there's already a gid
        if ([allGIDs containsObject:@(probeGID)])
        {
            // Yes: try the next number
            probeGID++;
        }
        else
        {
            // No: this GID is good to go
            found = YES;
        }
    }

    return @(probeGID);

}

ODRecord *FindGroupByName(ODNode *node, NSString *groupName)
{
    NSError *error;
    ODRecord *record = [node recordWithRecordType:kODRecordTypeGroups name:groupName attributes:nil error:&error];

    if (record == nil)
    {
        @throw [NSException exceptionWithName:@"Failed to find group" reason:@"Could not find group in OD" userInfo:error.userInfo];
    }

    return record;
}

ODRecord *FindOrCreateGroupByName(ODNode *node, NSString *groupName)
{
    NSError *error;
    BOOL success;
    ODRecord *record;

    // Find record
    record = [node recordWithRecordType:kODRecordTypeGroups name:groupName attributes:nil error:nil];

    // Found!
    if (record)
    {
        return record;
    }

    // Not found - create
    record = [node createRecordWithRecordType:kODRecordTypeGroups name:groupName attributes:nil error:&error];
    if (!record)
    {
        @throw [NSException exceptionWithName:@"OpenDirectory Error" reason:@"Could not create group" userInfo:error.userInfo];
    }
    [record synchronizeAndReturnError:nil];

    // Set GID
    success = [record setValue:[NextAvailableGroup(node) stringValue] forAttribute:kODAttributeTypePrimaryGroupID error:&error];
    if (!success)
    {
        @throw [NSException exceptionWithName:@"OpenDirectory Error" reason:@"Could not set group GID" userInfo:error.userInfo];
    }
    [record synchronizeAndReturnError:nil];

    // Set password placeholder
    success = [record setValue:@"*" forAttribute:kODAttributeTypePassword error:&error];
    if (!success)
    {
        @throw [NSException exceptionWithName:@"OpenDirectory Error" reason:@"Could not set group password" userInfo:error.userInfo];
    }
    [record synchronizeAndReturnError:nil];

    return record;

}

BOOL SetNestedGroupMembershipOfGroup(ODRecord *groupRecord, NSArray *arrayOfRecords)
{
    NSMutableArray *nestedGroupGUIDs = [NSMutableArray array];

    // Collect GUIDs
    for (id nestedGroup in arrayOfRecords)
    {
        NSString *nestedGroupGUID;

        if ([nestedGroup isKindOfClass:ODRecord.class])
        {
            nestedGroupGUID = [[nestedGroup valuesForAttribute:kODAttributeTypeGUID error:nil] objectAtIndex:0];
        }
        else
        {
            nestedGroupGUID = nestedGroup;
        }

        if (nestedGroupGUID)
        {
            [nestedGroupGUIDs addObject:nestedGroupGUID];
        }
        else
        {
            @throw [NSException exceptionWithName:@"Missing GUID" reason:@"Object was missing GUID" userInfo:@{@"group": nestedGroup} ];
        }
    }

    return SetNestedGroupMembershipOfGroupByGUIDs(groupRecord, nestedGroupGUIDs);
}

BOOL SetNestedGroupMembershipOfGroupByGUIDs(ODRecord *groupRecord, NSArray *arrayOfGUIDs)
{
    NSError *error;
    BOOL success;

    // Now set the NestedGroups
    success = [groupRecord setValue:arrayOfGUIDs forAttribute:kODAttributeTypeNestedGroups error:&error];
    return success;
}
