
#import <Foundation/Foundation.h>

// Drop-in replacement for NSLog, without the date/time/process info

void NSPrint(NSString *format, ...) {

    // Nothing? Print a newline and leave
    if (!format)
    {
        fputs("\n", stdout);
        return;
    }

    va_list arguments;
    va_start(arguments, format);

    // Add the newline
    NSString *formatWithNewline = [NSString stringWithFormat:@"%@\n", format];

    // Print
    fputs([[[NSString alloc] initWithFormat:formatWithNewline arguments:arguments] UTF8String], stdout);

    va_end(arguments);
}
